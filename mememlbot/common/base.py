"""Abstract class for MemeMLBot."""
import abc
import logging

from common import api
from common import cmd_parser


class MemeMLBot(abc.ABC):
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._api = api.MemeGeneratorAPI(
            host='api', port=6669, logger=self._logger
        )
        self._cmdp = self._get_default_cmd_parser()

    def _get_default_cmd_parser(self):
        cmdp = (
            cmd_parser.CommandParser()
            .with_prefix('!meme')
            .with_default(self._handle_unknown_command)
            .with_command(r'^help$', self._display_help)
            .with_command(
                r'^list\s(?P<lang>pl|en)\s*$',
                self._list_templates,
                args=['lang'],
            )
            .with_command(
                r'^(?:(?P<lang>pl|en)\s)?'
                r'(?P<upper>[^|]+)'
                r'(?:\s*\|\s*(?P<lower>.+))?$',
                self._get_meme,
                args=['lang', 'upper_text', 'lower_text'],
            )
        )

        return cmdp

    def _get_meme(self, lang, upper_text, lower_text, client, channel, user):
        self._logger.info(f'Get meme: {lang}, {upper_text}, {lower_text}')

        lang = lang or 'en'
        upper_text = upper_text.strip()
        lower_text = (lower_text or '').strip()

        meme_img_data = self._api.generate_meme(
            lang=lang,
            upper_text=upper_text,
            lower_text=lower_text
        )

        if meme_img_data is None:
            self.send_message(
                client=client,
                channel=channel,
                text=f'{self._mention(user)} I am sorry, but due to some '
                     f'magical API errors I could '
                     f'not create your requested meme :C'
            )
            return

        self.send_message(
            client=client,
            channel=channel,
            text=f'{self._mention(user)} Here is your meme ;)',
            attachment=meme_img_data
        )

    def _list_templates(self, lang, client, channel, user):
        templates = self._api.list_templates(lang)

        if templates is None:
            self.send_message(
                client=client,
                channel=channel,
                text=f'{self._mention(user)} I am sorry, but due to some '
                     f'magical API errors I could not list the templates :C'
            )
            return

        text = f'{self._mention(user)} Available templates:\n{templates}'
        self.send_message(client=client, channel=channel, text=text)

    def _display_help(self, client, channel, user):
        text = (f'{self._mention(user)} Available commands:\n'
                'help - Displays this message\n'
                'list <pl|en> - List meme templates for a given language\n'
                '<pl|en> <upper text> | <lower text> - Generates meme with '
                'given upper and lower text in the specified language. Note '
                'that, both the language (default: en) and lower text are '
                'optional.')
        self.send_message(client=client, channel=channel, text=text)

    def _handle_unknown_command(self, text, client, channel, user):
        text = (f'{self._mention(user)} Unknown command: "{text}". '
                f'Use "help" command to see available options.')
        self.send_message(client=client, channel=channel, text=text)

    def on_ready(self):
        self._logger.info('The bot is ready!')

    def on_message(self, client, channel, user, msg):
        self._logger.info(f'New message arrived: \"{msg}\" from {user}')

        if msg.startswith(self._cmdp.prefix):
            self._cmdp.parse(msg, client=client, channel=channel, user=user)

    @abc.abstractmethod
    def _mention(self, user):
        pass

    @abc.abstractmethod
    def send_message(self, client, channel, text, attachment=None):
        pass

    @abc.abstractmethod
    def run(self, token):
        pass

