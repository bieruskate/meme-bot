"""Connection to Meme Generator API."""
import io
import json

import requests


class MemeGeneratorAPI:
    def __init__(self, host, port, logger):
        self._host = host
        self._port = port
        self._logger = logger

    @property
    def _uri(self):
        return f'http://{self._host}:{self._port}'

    def generate_meme(self, lang, upper_text, lower_text):
        data = {
            'lang': lang,
            'upper_text': upper_text,
            'lower_text': lower_text,
        }

        self._logger.info(f'Trying to retrieve meme for text: {data}')

        res = requests.post(url=f'{self._uri}/meme', data=data)

        if res.status_code == 200:
            self._logger.info('Retrieved meme from API')
            img_data = io.BytesIO(res.content)
        else:
            self._logger.info('Could not retrieve meme from API')
            img_data = None

        return img_data

    def list_templates(self, lang):
        data = {
            'lang': lang,
        }

        self._logger.info(f'Trying to retrieve template list for lang: {lang}')

        res = requests.post(url=f'{self._uri}/templates', data=data)

        if res.status_code == 200:
            self._logger.info('Retrieved template list from API')
            templates = json.loads(res.content)['templates']
            templates = '\n'.join('- ' + t for t in templates)
        else:
            self._logger.info('Could not retrieve template list from API')
            templates = None

        return templates
