import io

import flask

import meme2vec as m2v

app = flask.Flask(__name__)


def _make_predictor(lang, text_proc_cls):
    return m2v.template_predictor.TemplatePredictor(
        text_preprocessor_cls=text_proc_cls,
        word_mappings_file='/data/%s/word-mappings.npy' % lang,
        grammar_mappings_file='/data/%s/grammar-mappings.pkl' % lang,
        nn_model_file='/data/%s/model.h5' % lang,
        class_mapping_file='/data/%s/class-mappings.pkl' % lang,
    )


tp = {
    'pl': _make_predictor('pl', m2v.preprocessor.WCRFTTextPreprocessor),
    'en': _make_predictor('en', m2v.preprocessor.SpacyTextPreprocessor),
}

img_gen = m2v.image_generator.ImageGenerator(
    templates_dir='/data/templates',
    font_path='/data/common/impact.ttf'
)


@app.route('/meme', methods=['POST'])
def generate_meme():
    lang = flask.request.form['lang']
    upper_text = flask.request.form['upper_text']
    lower_text = flask.request.form['lower_text']

    meme_template = tp[lang].predict_template(
        ' '.join([upper_text, lower_text])
    )

    meme_img = img_gen.generate_meme(
        template=meme_template,
        top_text=upper_text,
        bottom_text=lower_text
    )

    fp = io.BytesIO()
    meme_img.save(fp, 'PNG')

    fp.seek(0)

    return flask.send_file(
        filename_or_fp=fp,
        mimetype='image/png'
    )


@app.route('/templates', methods=['POST'])
def list_templates():
    lang = flask.request.form['lang']

    return flask.jsonify({'templates': tp[lang].templates})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6669, debug=True)
