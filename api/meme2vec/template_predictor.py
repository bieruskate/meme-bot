import pickle

from keras import models as ks_mod
from keras.preprocessing import sequence as ks_seq
import numpy as np
import tensorflow as tf


class TemplatePredictor:
    PADDING_LEN = 16

    def __init__(self, text_preprocessor_cls, word_mappings_file,
                 grammar_mappings_file, nn_model_file, class_mapping_file):
        self._nn_model = ks_mod.load_model(nn_model_file)
        self._graph = tf.get_default_graph()

        embs = np.load(word_mappings_file, allow_pickle=True).item()
        self._word_mappings = embs['mappings']

        with open(grammar_mappings_file, 'rb') as fh:
            grammar_mappings = pickle.load(fh)
        self._grammar_mappings = grammar_mappings

        with open(class_mapping_file, 'rb') as fh:
            class_mappings = pickle.load(fh)
        self._class_mappings = class_mappings

        self._text_proc = text_preprocessor_cls()

    def predict_template(self, text):
        lemmas, gram_cls = self._text_proc.run(text)

        lemmas = self._map_lemmas(lemmas)
        gram_cls = self._map_grammar_classes(gram_cls)

        with self._graph.as_default():
            pred_softmax = self._nn_model.predict([lemmas, gram_cls])

        label = np.argmax(pred_softmax)
        _, template = self._class_mappings[label]

        return template

    def _map_lemmas(self, lemmas):
        sequence = [self._word_mappings.get(word, -1) + 1 for word in lemmas]
        padded_seq = ks_seq.pad_sequences([sequence], maxlen=self.PADDING_LEN)
        return padded_seq

    def _map_grammar_classes(self, gram_cls):
        gram_cls = [self._grammar_mappings[cls] for cls in gram_cls]
        padded_seq = ks_seq.pad_sequences([gram_cls], maxlen=self.PADDING_LEN)
        return padded_seq

    @property
    def templates(self):
        return [v[0] for v in self._class_mappings.values()]
